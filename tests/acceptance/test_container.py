import os
import time
import docker
from docker import DockerClient

from tests.acceptance.consts import DOCKER_HOST


def docker_client() -> DockerClient:
    return DockerClient(base_url=DOCKER_HOST)


if __name__ == "__main__":
    client = docker_client()

    # Build the Docker image
    path = os.path.dirname(os.path.abspath(__file__))
    image, _ = client.images.build(path=path, tag="add_numbers_image")

    # Run a container from the image
    container = client.containers.run("add_numbers_image", detach=True)

    container.wait()

    # Print the container logs
    print(container.logs().decode("utf-8"))

    print("hello world")
