import pytest


@pytest.fixture(scope="function")
def hello_world():
    return "hello world"
