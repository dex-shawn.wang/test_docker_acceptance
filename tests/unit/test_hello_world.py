import pytest


def test_hello_world(hello_world):
    assert hello_world == "hello world"


@pytest.mark.parametrize("test_input, expected", [("3+5", 8), ("2+4", 6), ("6*9", 54)])
def test_eval(test_input, expected):
    assert eval(test_input) == expected


@pytest.fixture(scope="function")
def filenames(request: pytest.FixtureRequest) -> str:
    if hasattr(request, "param"):
        return request.param
    else:
        return "default_filenames.txt"


def test_filenames(filenames):
    assert filenames == "default_filenames.txt"


@pytest.mark.parametrize("filenames", ["test_filenames.txt"], indirect=True)
def test_filenames_parameterized(filenames):
    assert filenames == "test_filenames.txt"
